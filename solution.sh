#!/bin/bash
# nombre de ligne
wc -l wikirank-fr.tsv.gz

#nombre de ligne sans les doublons
sort -u wikirank-fr.tsv | wc -l

#trie inverse
sort -r wiki.txt > wik.txt

# suppression de la ligne page_id (1d correspond à la 1er ligne)
sed '1d' wik.txt

#les top 20 des min
sort -k 4 -t$'\t' -n -r  wiki.txt | head -20 > wiki_top_20_max.txt

#les top 20 des min
#l'option -k désigne la colonne du tri, -t spécifie le délimiteur et head sélectionne les 20 premières lignes
sort -k 4 -t$'\t' wiki.txt | head -20 > wiki_top_20_min.txt

# ou avec sort -k 4 -t'        ' wiki.txt | head -20 > wiki_top_20_min.txt
# on fait crtl+v tab ppour avoir la tabulation


#faire une correlation 
# selection de deux colonnes 4 et 5
#l'option -F permet de spécifier le délimiteur, $4 et $5 sont les colonnes en question séparer par \t
awk -F'\t' '{print $4 "\t" $5}' wiki_top_20_max.txt > correlation_max_point.txt
# utilisation de gnuplot

#tableau entre la popularité et le nombre d'auteur